This is a base framework to build your own theme.
It contains only 
   - some cross-browser fixes, 
   - and the most common regions,
   - 3 columns design (with 2 sidebars)
   - responsive design (to fit devices like phones, phablets and tablets)

It's unlikely that you want to use this theme as is, customize to enjoy.


IMPORTANT:

To ensure benefiting from updates without losing your customizations,
do not modify the provided files. Instead:
   - MAKE A COPRY of light_and_responsive.info and name as you want, like
     my_own_theme.info. Any other .info file than light_and_responsive.info
     will be ignored by updates and preserved.
   - ADD YOUR CSS FILES and other customizations in custom/ subfolder.
     Content of custom/ folder will be ignored by updates and preserved.
   - To disable entirely one of the existing .css files, just create one
     with the same name inside custom/ (empty or with your own code).


REGIONS

   Header
   - Leaderboard: on top of the page, full with
   - Header: part of the header next to the logo
   - Menu bar: under logo and header region

   Main Content
   - Highlight: shown before title and article
   - Help: after title, before article
   - Side content: could before article or a floating box
   - Content: after article, still
   - Secondary content: under Content, still between sidebars
   - First sidebar: on the left side, 
     	   	    moves under secondary content on very small screens
   - Second sidebar: on the left side, 
     	   	    moves under secondary content on small screens

   Bottom
   - Tertiary content, right under main content and sidebars, full width

   Footer
   - At bottom of the window, or after all other content if content is long


LAST WORD

This is a work in progress, based on the common things I use often in my theme
designs. It may not fit all situations and won't pretend to. I am open to 
suggestion, keeping in mind that lightness is the key word.
