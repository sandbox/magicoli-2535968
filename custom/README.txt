Put here your custom css. Il will not be overriden by theme updates.

Files 
      custom_base.css
      custom_fonts.css
      custom_layout.css
      custom_style.css
will be read by defaut.

File named like files contained in css/ folder will disable their 
default equivalent (base.css, fonts.css, layout.css, style.css).

Other files can be declared and must be declared in your .info file.
